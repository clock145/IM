#ifndef __IMDATA_H__
#define __IMDATA_H__

#include "CrossApp.h"

/*首页TabelBar*/
#define TABLE_BAR_SIZE      3

#define TABLE_BAR_LABEL_0  "首页"
#define TABLE_BAR_LABEL_1  "聊天"
#define TABLE_BAR_LABEL_2  "我"

#define NAVIGATION_BAR_ITEM_NAME_0   "热门"
#define NAVIGATION_BAR_ITEM_NAME_1   "最新"
#define NAVIGATION_BAR_ITEM_NAME_2   "聊天"
#define NAVIGATION_BAR_ITEM_NAME_3   "我"





/*图片资源*/

#define TABLE_BAR_IMAGE_0_NORMAL  "baby_genius/0_1.png"
#define TABLE_BAR_IMAGE_0_SELECTED  "baby_genius/0_2.png"
#define TABLE_BAR_IMAGE_1_NORMAL  "baby_genius/1_1.png"
#define TABLE_BAR_IMAGE_1_SELECTED  "baby_genius/1_2.png"
#define TABLE_BAR_IMAGE_2_NORMAL  "baby_genius/4_1.png"
#define TABLE_BAR_IMAGE_2_SELECTED  "baby_genius/4_2.png"

#define NAVIGATION_BAR_ITEM_SEARCH_NORMAL  "baby_genius/1_2.png"
#define NAVIGATION_BAR_ITEM_SEARCH_SELECTED  "baby_genius/1_1.png"
#define NAVIGATION_BAR_ITEM_ADD_NORMAL     "baby_genius/0_2.png"
#define NAVIGATION_BAR_ITEM_ADD_SELECTED     "baby_genius/0_1.png"


#endif /* __IMDATA_H__ */
